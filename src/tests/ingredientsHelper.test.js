import getMainHops from "../helpers/ingredientsHelper";

test("sorts hops by weight and returns top three as string", () => {
  const hops = [
    { name: "test 1", amount: { value: 5 } },
    { name: "test 2", amount: { value: 8 } },
    { name: "test 3", amount: { value: 2 } },
    { name: "test 4", amount: { value: 9 } }
  ];
  expect(getMainHops(hops)).toBe("test 4, test 2, test 1");
});
