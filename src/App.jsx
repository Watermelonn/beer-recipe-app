import React, { useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import axios from "axios";
import "./styles/header.scss";
import "./styles/beer.scss";
import Header from "./components/common/header";
import Beers from "./components/beer/beers";
import BeerDetail from "./components/beer/beerDetail";
import BeerContext from "./context/beerContext";

function App() {
  const [beers, setBeers] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [numberOfItemsPerPage] = useState(15);
  const [searchText, setSearchText] = useState("");

  useEffect(() => {
    axios("https://api.punkapi.com/v2/beers?per_page=80").then(response => {
      setBeers(
        response.data.filter(beer =>
          beer.name.toUpperCase().includes(searchText.toUpperCase())
        )
      );
    });
  }, [searchText]);

  return (
    <React.Fragment>
      <Header />
      <main className="container-lg">
        <div className="row mt-2">
          <div className="col">
            <BeerContext.Provider
              value={{
                beers,
                setBeers,
                pageNumber,
                setPageNumber,
                numberOfItemsPerPage,
                searchText,
                setSearchText
              }}
            >
              <Switch>
                <Route path="/beer/:id" component={BeerDetail} />
                <Route path="/" component={Beers} />
              </Switch>
            </BeerContext.Provider>
          </div>
        </div>
      </main>
    </React.Fragment>
  );
}

export default App;
