export default function getMainHops(hops) {
  const hopsInOrderOfWeight = hops.sort((a, b) =>
    a.amount.value < b.amount.value ? 1 : -1
  );
  const hopNames = hopsInOrderOfWeight.map(hop => hop.name);
  const uniqueHopNames = [...new Set(hopNames)];

  return uniqueHopNames.slice(0, 3).join(", ");
}
