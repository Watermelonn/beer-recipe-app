import React from "react";
import PropTypes from "prop-types";

const BeerDescriptionWithSubtitle = ({ title, description }) => {
  return (
    <>
      <p className="beer-subtitle">{title}</p>
      <span>{description}</span>
    </>
  );
};

BeerDescriptionWithSubtitle.propTypes = {
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

export default BeerDescriptionWithSubtitle;
