import React, { useEffect, useContext, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import BeerContext from "../../context/beerContext";
import BeerDescriptionWithSubtitle from "./misc/beerDescriptionWithSubtitle";

export default function BeerDetail(props) {
  const beerContext = useContext(BeerContext);
  const [selectedBeer, setSelectedBeer] = useState(null);

  useEffect(() => {
    setSelectedBeer(
      beerContext.beers.find(
        beer => beer.id === parseInt(props.match.params.id)
      )
    );
  }, [beerContext.beers, props.match.params.id]);

  const navigateToBeersList = () => {
    props.history.push("/");
  };

  if (!selectedBeer) return <></>;
  return (
    <>
      <div className="row">
        <div className="col">
          <button className="btn btn-secondary" onClick={navigateToBeersList}>
            <FontAwesomeIcon icon={faLongArrowAltLeft} /> Back To Beers
          </button>
        </div>
      </div>
      <div className="row">
        <div className="col pt-3">
          <span className="beer-title">{selectedBeer.name}</span>
          <span className="beer-subtitle pl-3">{`By: ${selectedBeer.contributed_by}`}</span>
        </div>
      </div>
      <div className="row">
        <div className="col-md-2">
          <img
            className="beer-image"
            alt={selectedBeer.name}
            src={selectedBeer.image_url}
          />
        </div>
        <div className="col-md-10">
          <BeerDescriptionWithSubtitle
            title="Description"
            description={selectedBeer.description}
          />
          <BeerDescriptionWithSubtitle
            title="Food Pairings"
            description={selectedBeer.food_pairing.join(", ")}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="Abv"
            description={`${selectedBeer.abv}%`}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="OG"
            description={selectedBeer.target_og}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="FG"
            description={selectedBeer.target_fg}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="Mash pH"
            description={selectedBeer.ph}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="SRM"
            description={selectedBeer.srm}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="IBU"
            description={selectedBeer.ibu}
          />
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="Volume"
            description={`${selectedBeer.boil_volume.value} ${selectedBeer.boil_volume.unit}`}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <p className="beer-subtitle">Malt</p>
          {selectedBeer.ingredients.malt.map(malt => (
            <span key={malt.name} className="beer-ingredient">
              {malt.name} - {malt.amount.value} {malt.amount.unit}
            </span>
          ))}
        </div>
        <div className="col">
          <p className="beer-subtitle">Hops</p>
          {selectedBeer.ingredients.hops.map(hop => (
            <span key={hop.name} className="beer-ingredient">
              {hop.name} - {hop.amount.value} {hop.amount.unit} ({hop.add})
            </span>
          ))}
        </div>
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="Yeast"
            description={selectedBeer.ingredients.yeast}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <BeerDescriptionWithSubtitle
            title="Brewers Tips"
            description={selectedBeer.brewers_tips}
          />
        </div>
      </div>
    </>
  );
}
