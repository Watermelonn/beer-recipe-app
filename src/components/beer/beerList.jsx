import React from "react";
import PropTypes, { object } from "prop-types";
import getMainHops from "../../helpers/ingredientsHelper";

export default function BeerList({ beers, onBeerClick }) {
  const tableHeadings = ["Name", "Abv", "Volume", "Hops"];

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          {tableHeadings.map(name => (
            <th key={name}>{name}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {beers.map(({ id, name, abv, volume, ingredients }) => (
          <tr
            key={name}
            className="beer-list-item"
            onClick={() => onBeerClick(id)}
          >
            <td>{name}</td>
            <td>{abv}%</td>
            <td>
              {volume.value} {volume.unit}
            </td>
            <td>{getMainHops(ingredients.hops)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

BeerList.propTypes = {
  beers: PropTypes.arrayOf(object).isRequired,
  onBeerClick: PropTypes.func.isRequired
};
