import React, { useContext } from "react";
import BeerList from "./beerList";
import Pagination from "../common/pagination";
import SearchPanel from "../common/searchPanel";
import BeerContext from "../../context/beerContext";

export default function Beers(props) {
  const beerContext = useContext(BeerContext);

  const getBeersToShow = () => {
    const { beers, numberOfItemsPerPage, pageNumber } = beerContext;

    let beersArray = [...beers];

    return beersArray.splice(
      numberOfItemsPerPage * (pageNumber - 1),
      numberOfItemsPerPage
    );
  };

  const viewBeerDetail = id => props.history.push(`/beer/${id}`);

  const searchBeers = searchText => {
    beerContext.setSearchText(searchText);
    beerContext.setPageNumber(1);
  };

  return (
    <>
      <SearchPanel onSearch={searchText => searchBeers(searchText)} />
      <BeerList
        beers={getBeersToShow()}
        onBeerClick={id => viewBeerDetail(id)}
      />
      <Pagination
        numberOfItems={beerContext.beers.length}
        numberOfItemsPerPage={beerContext.numberOfItemsPerPage}
        currentPageNumber={beerContext.pageNumber}
        onPageChange={pageNumber => beerContext.setPageNumber(pageNumber)}
      />
    </>
  );
}
