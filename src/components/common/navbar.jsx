import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="navbar navbar-light">
      <div className="container-lg">
        <Link to="/">
          <span className="navbar-brand mb-0 h1 logo">
            CraftBeerRecipes.org
          </span>
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
