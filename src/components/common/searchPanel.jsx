import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

function SearchPanel({ onSearch }) {
  return (
    <nav className="navbar navbar-light">
      <div className="container-lg pull-right pr-0">
        <div className="form-inline">
          <input
            className="form-control mr-sm-2"
            placeholder="Search For Beers"
            onChange={e => onSearch(e.target.value)}
          />
          <FontAwesomeIcon icon={faSearch} />
        </div>
      </div>
    </nav>
  );
}

SearchPanel.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export default SearchPanel;
