import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLongArrowAltLeft,
  faLongArrowAltRight
} from "@fortawesome/free-solid-svg-icons";
import "../../styles/pagination.scss";

const Pagination = ({
  numberOfItems,
  numberOfItemsPerPage,
  currentPageNumber,
  onPageChange
}) => {
  const numberOfPages = Math.ceil(numberOfItems / numberOfItemsPerPage);
  const pageNumbers = getPageNumberElements(
    numberOfPages,
    currentPageNumber,
    onPageChange
  );

  return (
    <nav className="pull-right">
      <ul className="pagination">
        <span className="pagination-info-label">
          Page {currentPageNumber} of {numberOfPages} ({numberOfItems} recipes)
        </span>
        <li
          className={`page-item ${currentPageNumber === 1 && "disabled"}`}
          onClick={() => onPageChange(currentPageNumber - 1)}
        >
          <span className="page-link">
            <FontAwesomeIcon icon={faLongArrowAltLeft} /> Previous
          </span>
        </li>
        {pageNumbers.map((element, index) => (
          <Fragment key={`pagination${index}`}>{element}</Fragment>
        ))}
        <li
          className={`page-item ${currentPageNumber === numberOfPages &&
            "disabled"}`}
          onClick={() => onPageChange(currentPageNumber + 1)}
        >
          <span className="page-link">
            Next <FontAwesomeIcon icon={faLongArrowAltRight} />
          </span>
        </li>
      </ul>
    </nav>
  );
};

const getPageNumberElements = (
  numberOfPages,
  currentPageNumber,
  onPageChange
) => {
  let elements = [];

  for (let x = 1; x <= numberOfPages; x++) {
    elements.push(
      <li
        className={`page-item ${x === currentPageNumber && "active"}`}
        onClick={() => onPageChange(x)}
      >
        <span className={"page-link"}>{x}</span>
      </li>
    );
  }

  return elements;
};

Pagination.propTypes = {
  numberOfItems: PropTypes.number.isRequired,
  numberOfItemsPerPage: PropTypes.number.isRequired,
  currentPageNumber: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default Pagination;
