import React from "react";

const BeerContext = React.createContext();

BeerContext.displayName = "BeerContext";

export default BeerContext;
