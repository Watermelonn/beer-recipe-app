FROM node:13.8.0-alpine

RUN mkdir -p /srv/app/beer-app/beer-app
WORKDIR /srv/app/beer-app

COPY package.json /srv/app/beer-app
COPY package-lock.json /srv/app/beer-app

RUN npm install

COPY . /srv/app/beer-app

CMD ["npm", "start"]